#include <stdio.h>
#include <stdlib.h>

#include "backsubst.h"

int backsubst(Matrix* X, Matrix* A, Matrix* B) {
	if(A->rows != A->columns)
		return BACKSUBST_INVALID_MATRIX;
	int n = A->rows;
	if(X->rows != n || X->columns != 1 || B->rows != n || B->columns != 1)
		return BACKSUBST_INVALID_MATRIX;
	for(int row = 0; row < n; row++) 
		X->data[row][0] = B->data[row][0];
	for(int row = n-1; row >= 0; row--) {
		if(A->data[row][row] == 0)
			return BACKSUBST_ZERO_DIAGONAL;
		for(int i = row+1; i < n; i++)
			X->data[row][0] -= A->data[row][i] * X->data[i][0];
		X->data[row][0] /= A->data[row][row];
	}
	return BACKSUBST_SUCCESS;
}
