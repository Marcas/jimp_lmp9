#ifndef BACKSUBST_H
#define BACKSUBST_H

#include "mat_io.h"

#define BACKSUBST_SUCCESS 0
#define BACKSUBST_ZERO_DIAGONAL 1
#define BACKSUBST_INVALID_MATRIX 2

/**
 * Rozwiązuje układ równań liniowych, przedstawionych w postaci macierzowej,
 * gdzie macierz wspócznników liniowych jest dolną macierzą trójkątną
 * 
 * @param X Macierz (n x 1) do której zostaną wpisane rozwiązania układu
 * @param A Macierz (dolna trójkątna n x n) współczynników liniowych układu 
 * @param B Macierz (n x 1) wyrazów wolnych układu 
 * 
 * @return
 * 	BACKSUBST_SUCCESS Układ równań został rozwiązany pomyślnie
 * 	BACKSUBST_ZERO_DIAGONAL W macierzy współczynników 
 * 		liniowych na diagonalu wystąpiło 0
 * 	BACKSUBST_INVALID_MATRIX Nieprawidłowy rozmiar którejś z macierzy
 **/


int backsubst(Matrix* X, Matrix* A, Matrix* B);

#endif
