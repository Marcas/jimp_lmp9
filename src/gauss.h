#ifndef GAUSS_H
#define GAUSS_H

#include "mat_io.h"

#define GAUSS_ELIM_SUCCESS 0
#define GAUSS_ELIM_DEGENERATE_MATRIX 1
#define GAUSS_ELIM_INVALID_MATRIX 2

/**
 * Przeprowadza eliminacje Gaussa na danych macierzach w celu rozwiązania 
 * układu równań liniowych. Wynikiem jest dolna macierz trójkątna A, 
 * oraz macierz B, przkształcona w sposób zachowujący rozwiązania
 * z pierwotnego układu równań
 * 
 * Zakładane jest że układ równań, tworzony przez dane macierze, 
 * ma dokładnie tyle rozwiązań, ile każda z macierzy ma wierszy
 *
 * @param A Macierz (n x n) współczynników liniowych układu
 * @param B (Macierz (n x 1) wyrazów wolnych układu 
 * 
 * @return
 *     GAUSS_ELIM_SUCCESS Eliminacja Gaussa przebiegła pomyślnie
 *     GAUSS_ELIM_DEGENERATE_MATRIX Osobliwa macierz współcznników liniowych
 *     GAUSS_ELIM_INVALID_MATRIX Nieprawidłowy rozmiar którejś z macierzy
 **/

int eliminate(Matrix *mat, Matrix *b);
void findRowToSwap(Matrix *mat, Matrix *b, int rowNum);
void swapRow(Matrix *mat, Matrix *b, int rowNum, int rowMax);
double scaleOfVector(Matrix *mat, Matrix *b, int rowNum);

#endif
