#ifndef MAT_IO_H
#define MAT_IO_H

typedef struct {
	int rows, columns;
	double **data;
} Matrix;

/**
 * Odczytuje macierz z pliku i zapisuje ją do struktury.
 * 
 * Elementy wieszy oraz informacje o ilościach kolumn i wierszy są
 * rozdzielone spacjami. Macierz musi być zapisana w formacie:
 * <ilość wierszy> <ilość kolumn> <wiersz 1> <wiersz 2>...
 *
 * @param file_name Nazwa pliku z macierzą
 *
 * @return Wskaźnik na wczytaną macierz, lub NULL gdy wczytywanie się niepowiodło
 **/
Matrix* readFromFile(char* file_name);

/**
 * Wypisuje ładnie sformatowaną macierz na standardowe wyjśćie
 *
 * @param matrix Wskaźnik na wyświetlaną macierz
 **/
void printToScreen(Matrix* matrix);

/**
 * Alokuje na stercie macierz o podanych wymiarach
 *
 * @param rows Ilość wierszy macierzy
 * @param columns Ilość kolumn macierzy
 *
 * @return Wskaźnik na zaalokowaną macierz, lub NULL gdy alokacja się niepowiodła
 **/
Matrix* createMatrix(int rows, int columns);

/**
 * Zwalnia pamięć zajmowaną przez macierz 
 *
 * @param matrix Wskaźnik na usuwaną macierz
 **/
void freeMatrix(Matrix* matrix);

#endif
