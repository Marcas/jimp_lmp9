#include "mat_io.h"

#include <stdio.h>
#include <stdlib.h>

Matrix* readFromFile(char* file_name) {
	FILE* file = fopen(file_name, "r");
	if(file == NULL) {
		fprintf(stderr, "Nie udalo sie otworzyc pliku (plik: '%s')\n", file_name);
		return NULL;
	}
	int rows, columns;
	if(fscanf(file, "%d %d", &rows, &columns) != 2) {
		fprintf(stderr, "Nie udalo sie odczytac informacji o rozmiarach macierzy (plik: '%s')\n", file_name);
		fclose(file);
		return NULL;
	}
	Matrix* matrix = createMatrix(rows, columns);
	if(matrix == NULL) {
		fprintf(stderr, "Nie udalo sie zaalokowac macierzy o rozmiarach %dx%d (plik: '%s')\n", rows, columns, file_name);
		fclose(file);
		return NULL;
	}
	for(int row = 0; row < rows; row++) {
		for(int column = 0; column < columns; column++) {
			if(fscanf(file, "%lf", &(matrix->data[row][column])) == 1) 
				continue;
			fprintf(stderr, "Nie udalo sie wczytac %d,%d elementu macierzy (plik: '%s')\n", row+1, column+1, file_name);
			fclose(file);
			freeMatrix(matrix);
			return NULL;
		}
	}
	fclose(file);
	return matrix;
}

void printToScreen(Matrix* matrix) {
	if(matrix == NULL) {
		printf("Pusta macierz\n");
		return;
	}
	printf("[\n");
	for(int row = 0; row < matrix->rows; row++) {
		printf("\t");
		for(int column = 0; column < matrix->columns; column++) {
			printf("%f\t", matrix->data[row][column]);
		}
		printf("\n");
	}
	printf("]\n");
}

Matrix* createMatrix(int rows, int columns) {
	if(rows <= 0 || columns <= 0)
		return NULL;
	Matrix* matrix = (Matrix*) malloc(sizeof(*matrix));
	if(matrix == NULL)
		return NULL;
	matrix->rows = rows;
	matrix->columns = columns;
	matrix->data = (double**) malloc(rows * sizeof(double*));
	if(matrix->data == NULL)
		return NULL;
	for(int row = 0; row < rows; row++) {
		matrix->data[row] = (double*) malloc(columns * sizeof(double));
		if(matrix->data[row] != NULL)
			continue;
		for(int allocatedRow = 0; allocatedRow < row; allocatedRow++) 
			free(matrix->data[allocatedRow]);
		free(matrix);
		return NULL;	
	}
	return matrix;
}

void freeMatrix(Matrix* matrix) {
	if(matrix == NULL)
		return;
	for(int row = 0; row < matrix->rows; row++) 
		free(matrix->data[row]);
	free(matrix);
}

