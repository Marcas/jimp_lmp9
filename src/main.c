#include <stdlib.h>
#include <stdio.h>

#include "gauss.h"
#include "backsubst.h"

int main(int argc, char** argv) {
	if(argc < 2) {
		fprintf(stderr, "%s: Poprawne uzycie: \n", argv[0]);
		fprintf(stderr, "\t%s <plik macierzy wspolczynnikow liniowych> <plik macierzy wyrazow wolnych>\n", argv[0]);
		return EXIT_FAILURE;
	}

	Matrix* A = readFromFile(argv[1]);
	if(A == NULL) {
		fprintf(stderr, "%s: Nie udalo sie wczytac macierzy wspolczynnikow liniowych\n", argv[0]);
		return EXIT_FAILURE;
	}
	Matrix* B = readFromFile(argv[2]);
	if(B == NULL) {
		fprintf(stderr, "%s: Nie udalo sie wczytac macierzy wyrazow wolnych\n", argv[0]);
		return EXIT_FAILURE;
	}
	
	printf("Wczytana macierz wspolczynnikow liniowych:\n");
	printToScreen(A);
	printf("Wczytana macierz wyrazow wolnych:\n");
	printToScreen(B);

	int gauss_elim_result;
	gauss_elim_result = eliminate(A, B);
	if(gauss_elim_result == 1) {
		fprintf(stderr, "%s: Eliminacja Gaussa nieudana: Osobliwa macierz wspolczynnikow liniowych\n", argv[0]);
		return EXIT_FAILURE;
	} else if(gauss_elim_result == 1) {
		fprintf(stderr, "%s: Eliminacja Gaussa nieudana: Niewlasciwe rozmiary miacierzy\n", argv[0]);
		return EXIT_FAILURE;
	}

	printf("Macierz wspolczynnikow liniowych po eliminacji gaussa:\n");
	printToScreen(A);
	printf("Macierz wyrazow wolnych po eliminacji gaussa:\n");
	printToScreen(B);

	Matrix* X = createMatrix(A->rows, 1);
	if(X == NULL) {
		fprintf(stderr, "%s: Nie udalo sie zaalokowac macierzy rozwiazan\n", argv[0]);
		return EXIT_FAILURE;
	}

	int backsubst_result;
	backsubst_result = backsubst(X, A, B);
	if(backsubst_result == BACKSUBST_ZERO_DIAGONAL) {
		fprintf(stderr, "%s: W macierzy współczynników liniowych na diagonalu wystąpiło 0\n", argv[0]);
		return EXIT_FAILURE;
	} else if(backsubst_result == BACKSUBST_INVALID_MATRIX) {
		fprintf(stderr, "%s: Nieprawidłowy rozmiar którejś z macierzy\n", argv[0]);
		return EXIT_FAILURE;
	}

	printf("Macierz rozwiazan:\n");
	printToScreen(X);

	return EXIT_SUCCESS;
}

