#include "gauss.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void eliminateRow(Matrix *mat, Matrix *b, int rowNum)
{
	for (int i = rowNum + 1; i < mat->rows; i++)
	{
		if (mat->data[rowNum][rowNum] == 0)
		{
			findRowToSwap(mat, b, rowNum);
		}
		double factor = mat->data[i][rowNum] / mat->data[rowNum][rowNum];

		for (int j = 0; j < mat->columns; j++)
		{
			mat->data[i][j] -= factor * mat->data[rowNum][j];
		}
		b->data[i][0] -= factor * b->data[rowNum][0];
	}
}

void findRowToSwap(Matrix *mat, Matrix *b, int rowNum)
{
	double max = 0;
	int rowMax = 0;

	for (int i = rowNum + 1; i < mat->rows; i++)
	{
		double scale = scaleOfVector(mat, b, i);

		if (scale > max)
		{
			max = scale;
			rowMax = i;
		}
	}

	swapRow(mat, b, rowNum, rowMax);
}

void swapRow(Matrix *mat, Matrix *b, int rowNum, int rowMax)
{
	for (int i = 0; i < mat->columns; i++)
	{
		double temp;
		temp = mat->data[rowNum][i];
		mat->data[rowNum][i] = mat->data[rowMax][i];
		mat->data[rowMax][i] = temp;
	}

	double temp;
	temp = b->data[rowNum][0];
	b->data[rowNum][0] = b->data[rowMax][0];
	b->data[rowMax][0] = temp;

}

double scaleOfVector(Matrix *mat, Matrix *b, int rowNum)
{
	double max = 0;

	for (int i = 0; i < mat->columns; i++)
	{
		if (max < fabs(mat->data[rowNum][i]))
		{
			max = fabs(mat->data[rowNum][i]);
		}
	}
	if (max < fabs(b->data[rowNum][0]))
	{
		max = fabs(b->data[rowNum][0]);
	}

	return mat->data[rowNum][rowNum] / max;
}

int eliminate(Matrix *mat, Matrix *b)
{
	for (int i = 0; i < mat->rows; i++)
	{
	eliminateRow(mat, b, i);
	}

	return GAUSS_ELIM_SUCCESS;
}
