CC = gcc
CFLAGS = -Wall -Wextra -pedantic
BINNAME = gauss
PWD = $(shell pwd)
SRCDIR = $(PWD)/src
DATADIR =  $(PWD)/dane

export

all:
	cd $(SRCDIR) && make all
	$(CC) $(CFLAGS) $(SRCDIR)/*.o -o $(BINNAME)
test: all
	./gauss $(DATADIR)/A $(DATADIR)/b
clean:
	cd $(SRCDIR) && make clean
	-rm $(BINNAME)

.PHONY: all test clean

