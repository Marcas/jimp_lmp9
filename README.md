# lmp9


## Project

Small program for calculating equations with matrix

## Use

Type yout equation inside Data folder:
A - coefficients of variables
b - constants terms

example:

2x + y = 3 <br>
3y + 2x = 1 <br>

A: <br>
2 2 <br>
2 1 2 3 <br>
b: <br>
2 1 <br>
3 1 <br>

## Contributors

Jakub Miętki <br>
Igor Kędzierawski (LloydPL)



